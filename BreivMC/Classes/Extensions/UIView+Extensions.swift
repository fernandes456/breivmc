import UIKit

// Mark: - Subviews
extension UIView {
    
    public func add(_ subviews: UIView...) {
        subviews.forEach(self.addSubview)
    }
    
    public func removeAllSubviews() {
        for view in subviews { view.removeFromSuperview() }
    }
}

// Mark: - Layout
extension UIView {
    
    public static func usingAutoLayout() -> Self {
        let anyUIView = self.init()
        anyUIView.translatesAutoresizingMaskIntoConstraints = false
        
        return anyUIView
    }
    
    public func usingAutoLayout() -> Self {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        return self
    }
    
    @available(iOS 9.0, *)
    public func pinToBounds(of view: UIView,
                            topConstant: CGFloat = 0,
                            leadingConstant: CGFloat = 0,
                            bottomConstant: CGFloat = 0,
                            trailingConstant: CGFloat = 0) {
        
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: view.topAnchor, constant: topConstant),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingConstant),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomConstant),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: trailingConstant),
            ])
    }
}
