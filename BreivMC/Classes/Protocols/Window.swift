import UIKit

protocol WindowType: AnyObject {
    
    var rootViewController: UIViewController? { get set }
    func makeKeyAndVisible()
    func asWindow() -> UIWindow
}

extension UIWindow: WindowType {
    
    public func asWindow() -> UIWindow {
        
        return self
    }
}
