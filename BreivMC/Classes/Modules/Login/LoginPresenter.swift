import ModuleArchitecture

protocol LoginPresenterDelegate: AnyObject {
    
}

final class LoginPresenter: Presenter, LoginPresenterType {
    
    weak var delegate: LoginPresenterDelegate?

    override func start() {
        //
    }
}
