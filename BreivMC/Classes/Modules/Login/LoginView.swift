import UIKit
import ModuleArchitecture
import SnapKit

final class LoginView: UIView, Component {
    
    // MARK: Views
    private let logoImageView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo")).usingAutoLayout()
        imageView.contentMode = UIView.ContentMode.scaleAspectFit
        return imageView
    }()
    
    private let textStackView: UIStackView = {
        let stackView = UIStackView.usingAutoLayout()
        stackView.axis = .vertical
        stackView.spacing = 0
        return stackView
    }()
    
    private let loginTextFiled: UITextField = {
        let textField = UITextField.usingAutoLayout()
        textField.placeholder = "Nome ou email"
        textField.backgroundColor = UIColor(hex: 0x2b4153)
        return textField
    }()
    
    private let passwordTextFiled: UITextField = {
        let textField = UITextField.usingAutoLayout()
        textField.isSecureTextEntry = true
        textField.placeholder = "Senha"
        textField.backgroundColor = UIColor(hex: 0x2b4153)
        return textField
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton(type: .custom).usingAutoLayout()
        button.setImage(UIImage(named: "loginButton"), for: .normal)
        return button
    }()
    
    private let forgotPasswordButton: UIButton = {
        let button = UIButton(type: .custom).usingAutoLayout()
        button.setTitle("Esqueci minha senha", for: .normal)
        return button
    }()
    
    // MARK: Inits
    required init?(coder aDecoder: NSCoder) { fatalError("Not implemented") }
    
    init() {
        super.init(frame: .zero)
        self.customizeInterface()
    }
}

extension LoginView {
    private func customizeInterface() {
        
        self.backgroundColor = UIColor(hex: 0x193044) // [gfsf] criar um constant para isso
        
        self.defineSubviews()
        self.defineSubviewsConstraints()
    }
    
    private func defineSubviews() {
        
        self.textStackView.addArrangedSubview(self.loginTextFiled)
        self.textStackView.addArrangedSubview(self.passwordTextFiled)
        
        self.add(self.logoImageView,
                 self.textStackView,
                 self.loginButton,
                 self.forgotPasswordButton)
    }
    
    private func defineSubviewsConstraints() {
        
        self.logoImageView.snp.makeConstraints { maker in
            maker.width.equalTo(155)
            maker.height.equalTo(110)
//            maker.top.equalToSuperview().inset(48)
//            maker.centerX.equalToSuperview()
        }
        
        self.loginTextFiled.snp.makeConstraints { maker in
            maker.top.trailing.leading.equalToSuperview()
            maker.height.equalTo(64)
        }
        
        self.passwordTextFiled.snp.makeConstraints { maker in
            maker.bottom.trailing.leading.equalToSuperview()
            maker.height.equalTo(64)
        }
        
        self.textStackView.snp.makeConstraints { [unowned self] maker in
            maker.leading.trailing.equalToSuperview().inset(24)
            maker.top.equalTo(self.logoImageView.snp.bottom).inset(-40)
        }
        
        self.loginButton.snp.makeConstraints { [unowned self] maker in
            maker.leading.trailing.equalToSuperview().inset(24)
            maker.bottom.equalTo(self.forgotPasswordButton.snp.top).inset(-24)
            maker.height.equalTo(56)
        }
        
        self.forgotPasswordButton.snp.makeConstraints { maker in
            maker.leading.trailing.equalToSuperview().inset(24)
            maker.bottom.equalToSuperview().inset(40)
            maker.height.equalTo(20)
        }
    }
}

extension LoginView {
    
    enum Configuration {
        case build
        case resetAnimation
        case startAnimation
        case startErrorAnimation
    }
    
    func render(configuration: Configuration) {
        switch configuration {
        case .build:
            print("build")
            
        case .resetAnimation:
            self.resetAnimation()
            
        case .startAnimation:
            self.startAnimation()
            
        case .startErrorAnimation:
            print("error")
        }
    }
}

private extension LoginView {
    
    func resetAnimation() {
        self.textStackView.alpha = 0
        self.loginButton.alpha = 0
        self.forgotPasswordButton.alpha = 0
        self.logoImageView.snp.makeConstraints { maker in
            maker.center.equalToSuperview()
        }
        
        self.forgotPasswordButton.snp.updateConstraints { maker in
            maker.bottom.equalToSuperview().inset(0)
        }
        self.layoutIfNeeded()
    }
    
    func startAnimation() {

        self.logoImageView.snp.remakeConstraints { maker in
            maker.top.equalToSuperview().inset(48)
            maker.centerX.equalToSuperview()
        }
        
        UIView.animate(withDuration: 4, delay: 0, options: .curveEaseIn, animations: {
            self.textStackView.alpha = 1
            self.layoutIfNeeded()
        })
        
        self.forgotPasswordButton.snp.updateConstraints { maker in
            maker.bottom.equalToSuperview().inset(40)
        }
        
        UIView.animate(withDuration: 3, delay: 1, options: .curveEaseIn, animations: {
            self.loginButton.alpha = 1
            self.forgotPasswordButton.alpha = 1
            self.layoutIfNeeded()
        })
    }
}
