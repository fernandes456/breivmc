import ModuleArchitecture

final class LoginModule: Module, LoginModuleType {
    
    func createCoordinator(window: UIWindow) -> LoginCoordinatorType {
        
        let presenter = LoginPresenter()
        let viewController = LoginViewController()
        let coordinator = LoginCoordinator(window: window,
                                           presenter: presenter,
                                           viewController: viewController)
        presenter.delegate = coordinator
        return coordinator
    }
}
