import UIKit

protocol LoginViewControllerDelegate: AnyObject {
    
}

final class LoginViewController: UIViewController, LoginViewControllerType {

    weak var delegate: LoginViewControllerDelegate?
    private lazy var component = LoginView()
    
    override func loadView() {
        self.view = self.component
    }
    
    // [gfsf] tem que começar a animação no presenter?
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.render()
    }
}

extension LoginViewController: LoginPresenterView {
    func render() {
        self.component.render(configuration: .build)
        self.component.render(configuration: .resetAnimation)
        self.component.render(configuration: .startAnimation)
    }
    
    func showLoading() {
        
    }
    
    func hideLoading() {

    }
    
    func showLoginError() {
        self.component.render(configuration: .startErrorAnimation)
    }
}
