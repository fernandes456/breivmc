import ModuleArchitecture

final class LoginCoordinator: Coordinator<LoginPresenterType>, LoginCoordinatorType {

    private let window: WindowType
    private let viewController: LoginViewControllerType
    
    init(window: WindowType,
        presenter: LoginPresenterType,
        viewController: LoginViewControllerType) {
        
        self.window = window
        self.viewController = viewController
        super.init(presenter: presenter)
    }
    
    override func start() {
        super.start()
        self.window.asWindow().switchRootViewController(self.viewController.asViewController())
        self.window.makeKeyAndVisible()
    }
}

extension LoginCoordinator: LoginPresenterDelegate {
    
}
